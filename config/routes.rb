Rails.application.routes.draw do
	devise_for :users
	root 'posts#index'
	resources :users do
		get '/posts/:post_id/like' => 'likes#like', as: 'like'
		delete '/posts/:post_id/dislike' => 'likes#dislike', as: 'dislike'
		post 'following' => 'users#following', as: 'following'
		get 'followers' => 'users#followers', as: 'followers'
		resources :posts do
			resources :comments
			resources :likes
		end
	end
	resources :users
	resources :posts
end

