class Post < ApplicationRecord
	has_many_attached :images
	belongs_to :user
	has_many :comments, dependent: :destroy
	has_many :likes
	has_many :users, through: :likes
end
