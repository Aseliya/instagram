class PostsController < ApplicationController
	def index
		@posts = Post.all
		@user = current_user

	end

	def new
		@post = Post.new
		@user = User.find(params[:user_id])
	end

	def show
		@post = Post.find(params[:id])
	end

	def create
		@post = current_user.posts.build(post_params)
		@user = User.find(params[:user_id])

		if @post.save
			redirect_to user_posts_path
		else
			render 'new'
		end
	end

	def post_params
      params.require(:post).permit(:description, :user_id, :comment_id, images: [])
	end
end



