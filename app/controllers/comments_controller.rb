class CommentsController < ApplicationController
	before_action :set_post

	def new
		@comment = Comment.new
		@user = User.find(params[:user_id])
		@post = Post.find(params[:post_id])
	end

	def create
		@comment = @post.comments.build(comment_params)
		@comment.user_id = current_user.id



		if @comment.save
			flash[:success] = "You commented the hell out of that post!"
			redirect_back(fallback_location: root_path)
		else
			flash[:alert] = "Check the comment form, something went horribly wrong."
			render root_path
		end
	end

	private

	def comment_params
		params.require(:comment).permit(:content, :post_id, :user_id)
	end

	def set_post
		@post = Post.find(params[:post_id])
	end
end
