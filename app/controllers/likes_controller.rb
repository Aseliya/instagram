class LikesController < ApplicationController
	def like
		@post = Post.find(params[:post_id])
		@user = current_user.id
		@like = @post.likes.create(user: current_user)
		redirect_back(fallback_location: root_path)
	end

	def dislike
		@post = Post.find(params[:post_id])
		@user = current_user
		@likes = @post.likes.all
		@like = @likes.where(user_id: @user.id)
		@like.delete(@like)
		redirect_back(fallback_location: root_path)
	end
end
